﻿namespace Application.Core.ProjectAggregate;

public enum ProjectStatus
{
  InProgress,
  Complete
}
